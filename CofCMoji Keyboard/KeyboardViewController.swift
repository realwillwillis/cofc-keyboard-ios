//
//  KeyboardViewController.swift
//  CollegeOfCharlestonKeyboard
//
//  Created by Evgeniy Tkachenko on 19.12.16.
//  Copyright © 2016 BigDig. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import Kingfisher
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper
import ImageIO
import ObjectMapper
import Crashlytics

class KeyboardViewController: UIInputViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    private let Width = UIScreen.main.bounds.size.width
    
    var isTapLetter: Bool = false
    
    var btLayoutConstraint: NSLayoutConstraint!
    var firstOpen: Bool!

    @IBOutlet var btNextKeyboard: UIButton!
    @IBOutlet var btNextKeyboard2: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var vwQwertyKeyboard: UIView!
    
    var vwQwertyKeyboardNew: QwertyKeyboardView!
    
    var message: MessageView!
    
    var selectedCategories: Int = 0 {
        didSet {
            self.collectionView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: false)
            self.collectionView.reloadData()
        }
    }
    var countCategories: Int = 0
    
    var stickersOfCategory: [Int: [Int]] = [:]
    var arrayButtonsByCategory: [UIButton] = [UIButton]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let standartDefaults: UserDefaults = UserDefaults()
        if standartDefaults.string(forKey: "firstOpen") != "true" {
            standartDefaults.set("true", forKey: "firstOpen")
            standartDefaults.synchronize()
            self.saveStaticSticker()
        }
        
        let realm = try! Realm()
        let times = realm.objects(LastUpdate.self)
        if times.count == 0 {
            let update = LastUpdate()
            try! realm.write {
                realm.add(update, update: true)
            }
        }
        
        self.message = MessageView(screen: self, message: "Please, allow full access in your iPhone settings (see tutorial)", notHide:true)
        
        firstOpen = true
        
        self.vwQwertyKeyboard.isHidden = true
        
        
        //self.btNextKeyboard.addTarget(self, action: #selector(handleInputModeList(from:with:)), for: .allTouchEvents)
        //let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress))
        //self.btNextKeyboard.addGestureRecognizer(longPress)
        
        
        self.btNextKeyboard.isSelected = false
        self.btNextKeyboard.addTarget(self, action: #selector(self.showVwQwertyKeyboardNew), for: .touchUpInside)
        self.btNextKeyboard.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
        self.btNextKeyboard.imageView?.contentMode = .scaleAspectFit
        
        //self.setupQWERTYKeyboard()
        
        
        self.scrollView.contentSize = CGSize(width: 0, height: 35)
        
        self.saveInfo()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        
        
        var desiredHeight:CGFloat!
        desiredHeight = 216
        
        if (self.btLayoutConstraint != nil){
            view.removeConstraint(self.btLayoutConstraint)
            self.btLayoutConstraint = nil
        }
        self.btLayoutConstraint = NSLayoutConstraint(item: view,
                                                     attribute: .height,
                                                     relatedBy: .equal,
                                                     toItem: nil,
                                                     attribute: .notAnAttribute,
                                                     multiplier: 1,
                                                     constant: desiredHeight)
        self.btLayoutConstraint.priority = UILayoutPriority(UILayoutPriorityRequired)
        
        view.addConstraint(self.btLayoutConstraint)
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressed(sender:)))
        longPressRecognizer.minimumPressDuration = 2.0 // 1 second press
        longPressRecognizer.allowableMovement = 200 // 15 points
        self.btBS.addGestureRecognizer(longPressRecognizer)
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tap(sender:)))
        self.btBS.addGestureRecognizer(tap)
    }
    
    func longPressed(sender: UILongPressGestureRecognizer) {
        print("longpressed")
        textDocumentProxy.adjustTextPosition(byCharacterOffset: 1)
        textDocumentProxy.deleteBackward()
    }
    
    func tap(sender: UITapGestureRecognizer) {
        print("tap")
        textDocumentProxy.adjustTextPosition(byCharacterOffset: 1)
        textDocumentProxy.deleteBackward()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.getStickers()
        self.showBtLastStickers()
        
        if !self.isOpenAccessGranted() {
            self.message.show()
        }
        
        self.setupQwertyKeyboard()
    }
    
    func saveStaticSticker() {
        if let urlpath = Bundle.main.path(forResource: "StickersJSON", ofType: "json") {
            let url = NSURL.fileURL(withPath: urlpath)
            
            Alamofire.request(url.absoluteString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseObject { (response: DataResponse<CategoriesAndStickersResponse>) in
                
                if let error = response.result.error {
                    print(error.localizedDescription)
                } else if let timeLastUpdateResponse = response.result.value {
                    let categories = timeLastUpdateResponse.categories
                    let stickers = timeLastUpdateResponse.stickers
                    for category in categories {
                        let realm = try! Realm()
                        try! realm.write {
                            realm.add(category, update: true)
                        }
                    }
                    for sticker in stickers {
                        let type = sticker.iconUrl.components(separatedBy: ".").last!
                        sticker.type = type
                        let realm = try! Realm()
                        try! realm.write {
                            realm.add(sticker, update: true)
                        }
                    }
                }
            }
        }
    }
    
    func longPress(gesture: UILongPressGestureRecognizer) {
        if gesture.state == UIGestureRecognizerState.ended {
            print("Long Press")
            self.advanceToNextInputMode()
        }
        
    }
    
    func showQWERTYKeyboard() {
        if let view = self.message.view {
            self.message.view.isHidden = self.vwQwertyKeyboard.isHidden
            self.message.isHidden = self.vwQwertyKeyboard.isHidden
        }
        
        self.btNextKeyboard.isSelected = !self.btNextKeyboard.isSelected
        if self.btNextKeyboard.isSelected {
            for bt in self.arrayButtonsByCategory {
                bt.backgroundColor = UIColor.clear
            }
        } else {
            if let bt = self.arrayButtonsByCategory.first {
                self.tapCategory(sender: bt)
            }
        }
        self.vwQwertyKeyboard.isHidden = !self.btNextKeyboard.isSelected
        
//        var desiredHeight:CGFloat!
//        desiredHeight = view.frame.height
//        if self.vwQwertyKeyboard.isHidden {
//            desiredHeight = desiredHeight + 35
//        }else{
//            desiredHeight = desiredHeight - 35
//        }
//        
//        if (self.btLayoutConstraint != nil){
//            view.removeConstraint(self.btLayoutConstraint)
//            self.btLayoutConstraint = nil
//        }
//        self.btLayoutConstraint = NSLayoutConstraint(item: view,
//                                              attribute: .height,
//                                              relatedBy: .equal,
//                                              toItem: nil,
//                                              attribute: .notAnAttribute,
//                                              multiplier: 1,
//                                              constant: desiredHeight)
//        self.btLayoutConstraint.priority = UILayoutPriority(UILayoutPriorityRequired)
//        
//        view.addConstraint(self.btLayoutConstraint)
    }
    
    func saveInfo() {
        
//        if let date = UserDefaults.standard.object(forKey: "lastUpdate")
//        {
//            let difference = NSDate().timeIntervalSince(date as! Date)
//            if difference > 24*60*60 {
//                UserDefaults.standard.set(NSDate(), forKey: "lastUpdate")
//                UserDefaults.standard.synchronize()
//                ServerKeyboard.sharedInstance().getAllCategoriesAndStickers(completionBlock: { (success, error, categories, stickers) in
//                    if success {
//                        for category in categories {
//                            let realm = try! Realm()
//                            try! realm.write {
//                                realm.add(category, update: true)
//                            }
//                        }
//                        for sticker in stickers {
//                            let type = sticker.iconUrl.components(separatedBy: ".").last!
//                            sticker.type = type
//                            if !sticker.isHide {
//                                let realm = try! Realm()
//                                try! realm.write {
//                                    realm.add(sticker, update: true)
//                                }
//                            }
//                        }
//                        
//                        self.saveCategories()
//                        self.saveStickers()
//                    } else {
//                        //                MessageView(screen: self, message: error!.localizedDescription).show()
//                    }
//                })
//            }else{
//                self.saveCategories()
//                self.saveStickers()
//            }
//        }else{
//            let currentDate = NSDate()
//            UserDefaults.standard.set(currentDate, forKey: "lastUpdate")
//            UserDefaults.standard.synchronize()
        /*
        URLSession.shared.dataTask(with: url!) {(data, response, error) in
            debugPrint(data)
            
            debugPrint(response)
            
            debugPrint(error)
            }.resume()
        */
        
        let realm = try! Realm()
        if let times = realm.objects(LastUpdate.self).first {
            Alamofire.request("https://api.classatcofc.com/stickers/time?time=\(times.time)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseObject { (response: DataResponse<CategoriesAndStickersResponse>) in
                
                if let error = response.result.error {
                    print(error.localizedDescription)
                    self.getStickers()
                    self.saveCategories()
                    self.saveStickers()
                } else if let timeLastUpdateResponse = response.result.value {
                    
                    let realm = try! Realm()
                    let update = LastUpdate()
                    update.time = Int(Date().timeIntervalSince1970)
                    try! realm.write {
                        realm.add(update, update: true)
                    }
                    
                    let categories = timeLastUpdateResponse.categories
                    let stickers = timeLastUpdateResponse.stickers
                    for category in categories {
                        let realm = try! Realm()
                        try! realm.write {
                            realm.add(category, update: true)
                        }
                    }
                    for sticker in stickers {
                        let type = sticker.iconUrl.components(separatedBy: ".").last!
                        sticker.type = type
                        
                        if type == "png" {
                            let name = "sticker\(sticker.id).png"
                            let fileManager = FileManager.default
                            let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(name)
                            
                            do {
                                try fileManager.removeItem(atPath: paths)
                            } catch let error as NSError {
                                print(error.debugDescription)
                            }
                        } else {
                            var name = "sticker\(sticker.id).gif"
                            let fileManager = FileManager.default
                            var paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(name)
                            
                            do {
                                try fileManager.removeItem(atPath: paths)
                            } catch let error as NSError {
                                print(error.debugDescription)
                            }
                            
                            name = "stickerThumb\(sticker.id).gif"
                            paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(name)
                            
                            do {
                                try fileManager.removeItem(atPath: paths)
                            } catch let error as NSError {
                                print(error.debugDescription)
                            }
                        }
                        
                        let realm = try! Realm()
                        try! realm.write {
                            realm.add(sticker, update: true)
                        }
                    }
                    self.getStickers()
                    self.saveCategories()
                    self.saveStickers()
                }
            }
        }
 
        /*
        
            ServerKeyboard.sharedInstance().getAllCategoriesAndStickers(completionBlock: { (success, error, categories, stickers) in
                if success {
                    for category in categories {
                        let realm = try! Realm()
                        try! realm.write {
                            realm.add(category, update: true)
                        }
                    }
                    for sticker in stickers {
                        let type = sticker.iconUrl.components(separatedBy: ".").last!
                        sticker.type = type
                        let realm = try! Realm()
                        try! realm.write {
                            realm.add(sticker, update: true)
                        }
                    }
                    self.getStickers()
                    self.saveCategories()
                    self.saveStickers()
                } else {
                    self.getStickers()
                    self.saveCategories()
                    self.saveStickers()
                }
            })
        */
//        }
 
    }
    
    func saveCategories() {
        let realm = try! Realm()
        let categories = realm.objects(Category.self)
        
        for category in categories {
            if !category.isHide {
                if let url = URL(string: "https://api.classatcofc.com/" + category.iconUrl) {
                    self.saveImageDocumentDirectory(url: url, name: "category\(category.id).png", completion: { (success) in
//                        self.perform(#selector(self.addButtonsByCategory), with: self, afterDelay: 0)
                        if success == true {
//                            self.showBtLastStickers();
                        }
                    })
                }
            }
        }
        self.perform(#selector(self.showBtLastStickers), with: self, afterDelay: 1)
//        self.perform(#selector(self.showBtLastStickers), with: self, afterDelay: 4)
    }
    
    func saveStickers() {
        let realm = try! Realm()
        let stickers = realm.objects(Sticker.self)
        for sticker in stickers {
            if !sticker.isHide {
                let type = sticker.iconUrl.components(separatedBy: ".").last!
                if type != "gif" {
                    if let url = URL(string: "https://api.classatcofc.com/" + sticker.iconUrl) {
                        self.saveImageDocumentDirectory(url: url, name: "sticker\(sticker.id).png", completion: { (success) in
                            if success {
                            }
                        })
                    }
                } else {
                    if let url = URL(string: "https://api.classatcofc.com/" + sticker.iconThumbUrl) {
                        self.saveImageDocumentDirectory(url: url, name: "stickerThumb\(sticker.id).gif", completion: { (success) in
                            if success {
                                
                            }
                        })
                    }
                    if let url = URL(string: "https://api.classatcofc.com/" + sticker.iconUrl) {
                        self.saveImageDocumentDirectory(url: url, name: "sticker\(sticker.id).gif", completion: { (success) in
                            if success {
                                
                            }
                        })
                    }
                }
            }
        }
    }
    
    func getStickers() {
        let realm = try! Realm()
        let stickers = realm.objects(Sticker.self)
    
        let stickersSort = stickers.sorted {
            $0.serialNumber < $1.serialNumber
        }
        
        self.stickersOfCategory = [:]
        
        for sticker in stickersSort {
            if !sticker.isHide {
                if self.stickersOfCategory[sticker.categoryId] != nil {
                    self.stickersOfCategory[sticker.categoryId]!.append(sticker.id)
                } else {
                    self.stickersOfCategory[sticker.categoryId] = [sticker.id]
                }
            }
        }
        print(self.stickersOfCategory)
    }
    
    // MARK: - Image
    
    func saveImageDocumentDirectory(url: URL, name: String, completion: @escaping (_ success: Bool) -> ()) {
        self.getDataFromUrl(url: url) { (data, URLResponse, error) in
            if let data = data {
                print("download \(name)")
                let fileManager = FileManager.default
                let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(name)
                
                fileManager.createFile(atPath: paths as String, contents: data, attributes: nil)
                completion(true)
            } else {
                //MessageView(screen: self, message: error!.localizedDescription).show()
                completion(false)
            }
        }
    }
    
    func getImage(name: String) -> URL? {
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent(name)
//        let fileManager = FileManager.default
//        if fileManager.fileExists(atPath: imagePAth) {
            let url = URL.init(fileURLWithPath: imagePAth)
            return url
//        } else {
//            return nil
//        }
    }
    
    func getSizeImage(name: String) -> CGSize? {
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent(name)
        if fileManager.fileExists(atPath: imagePAth) {
            let image = UIImage(contentsOfFile: imagePAth)
            return image?.size
        } else {
            print("No Image")
            return nil
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> ()) {
        URLSession.shared.dataTask(with: url) {(data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func clearAllImage() {
        let fileManager = FileManager.default
        let documentsUrl = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first! as NSURL
        let documentsPath = documentsUrl.path
        
        do {
            if let documentPath = documentsPath {
                let fileNames = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
                print("all files in cache: \(fileNames)")
                for fileName in fileNames {
                    
                    if (fileName.hasSuffix(".png")) {
                        let filePathName = "\(documentPath)/\(fileName)"
                        try fileManager.removeItem(atPath: filePathName)
                    }
                }
                
                let files = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
                print("all files in cache after deleting images: \(files)")
            }
            
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
    
    //MARK: - Buttons by Category
    
    func addButtonsByCategory() {
        
//        let view = UIView()
        
//        for view:UIView! in self.scrollView.subviews {
//            if view != nil {
//                view.removeFromSuperview()
//            }
//        }
        
        self.scrollView.contentSize.width = 0
        if self.getLastStickersCount() == 0 {
            
            let realm = try! Realm()
            let categories = realm.objects(Category.self)
            
            let categoriesSort = categories.sorted {
                $0.sort < $1.sort
            }
            
            var number = 0
            for category in categoriesSort {
                if !category.isHide {
                    number += 1
                }
            }
            
            var delta = (self.scrollView.frame.size.width - CGFloat(number)*40)/2
            var width = self.scrollView.frame.size.width/CGFloat(number)
            if delta < 0 {
                width = 40
            }
            delta = 0
            
 
            var i = 0
            for category in categoriesSort {
                if !category.isHide {
                    let button = UIButton()
                    button.frame = CGRect(x: delta + CGFloat(i) * width, y: 0, width: width, height: 42)
                    
                    if let size = self.getSizeImage(name: "category\(category.id).png") {
                        let processor = ResizingImageProcessor(targetSize: CGSize(width: size.width, height: size.height))
                        
                        if let url = self.getImage(name: "category\(category.id).png") {
                            do {
                                _ = try Data(contentsOf: url)
                                button.kf.setImage(with: url, for: .normal, placeholder: nil, options: [.processor(processor)], progressBlock: nil, completionHandler: nil)
                                button.kf.setImage(with: url, for: .selected, placeholder: nil, options: [.processor(processor)], progressBlock: nil, completionHandler: nil)
                            } catch {
                                print(error.localizedDescription)
                            }
                        }
                    } else {
                        button.setImage(UIImage(named: "category\(category.id)"), for: .normal)
                        button.setImage(UIImage(named: "category\(category.id)"), for: .selected)
                    }
                    button.tag = category.id
                    button.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
                    button.imageView?.contentMode = .scaleAspectFit
                    button.addTarget(self, action: #selector(self.tapCategory(sender:)), for: .touchUpInside)
                    self.arrayButtonsByCategory.append(button)
                    self.scrollView.addSubview(button)
                    self.scrollView.contentSize.width += width
                    
                    if i == self.selectedCategories {
                        button.backgroundColor = UIColor(red: 0.31, green: 0.11, blue: 0.07, alpha: 1.0)
                    }
                    
                    i += 1
                }
            }
        } else {
            let realm = try! Realm()
            let categories = realm.objects(Category.self)
            
            let categoriesSort = categories.sorted {
                $0.sort < $1.sort
            }
            
            var number = 0
            for category in categoriesSort {
                if !category.isHide {
                    number += 1
                }
            }
            number+=1
            
            var delta = (self.scrollView.frame.size.width - CGFloat(number)*40)/2
            var width = self.scrollView.frame.size.width/CGFloat(number)
            if delta < 0 {
                width = 40
            }
            delta = 0
            
            let button = UIButton()
            button.frame = CGRect(x: delta + 0, y: 0, width: width, height: 42)
            button.tag = 100000
            button.setImage(UIImage(named: "Bt-Recently"), for: .normal)
            button.setImage(UIImage(named: "Bt-Recently"), for: .selected)
            button.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
            button.imageView?.contentMode = .scaleAspectFit
            button.addTarget(self, action: #selector(self.tapCategory(sender:)), for: .touchUpInside)
            self.arrayButtonsByCategory.append(button)
            self.scrollView.addSubview(button)
            self.scrollView.contentSize.width += width
            
            var i = 1
            for category in categoriesSort {
                if !category.isHide {
                    let button = UIButton()
                    
                    button.frame = CGRect(x: delta + CGFloat(i) * width, y: 0, width: width, height: 42)
                    button.tag = category.id
                    
                    if let size = self.getSizeImage(name: "category\(category.id).png") {
                        let processor = ResizingImageProcessor(targetSize: CGSize(width: size.width, height: size.height))
                    
                        
                        if let url = self.getImage(name: "category\(category.id).png") {
                            do {
                                let data = try Data(contentsOf: url)
                                button.kf.setImage(with: url, for: .normal, placeholder: nil, options: [.processor(processor)], progressBlock: nil, completionHandler: nil)
                                button.kf.setImage(with: url, for: .selected, placeholder: nil, options: [.processor(processor)], progressBlock: nil, completionHandler: nil)
                            } catch {
                                print(error.localizedDescription)
                            }
                        }
                    } else {
                        button.setImage(UIImage(named: "category\(category.id)"), for: .normal)
                        button.setImage(UIImage(named: "category\(category.id)"), for: .selected)
                    }
                    
                    button.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
                    button.imageView?.contentMode = .scaleAspectFit
                    button.addTarget(self, action: #selector(self.tapCategory(sender:)), for: .touchUpInside)
                    self.arrayButtonsByCategory.append(button)
                    self.scrollView.addSubview(button)
                    self.scrollView.contentSize.width += width
                    
                    if i == self.selectedCategories {
                        button.backgroundColor = UIColor(red: 0.31, green: 0.11, blue: 0.07, alpha: 1.0)
                    }
                    
                    i += 1
                }
            }
        }
        if self.vwQwertyKeyboard.isHidden {
//            if let bt = self.arrayButtonsByCategory.first {
//                if bt.isSelected{
//                    self.tapCategory(sender: bt)
//                }
//            }
            
            
            if firstOpen == true {
                firstOpen = false
                if let bt = self.arrayButtonsByCategory.first {
                    self.tapCategory(sender: bt)
                }
            } else {
                for bt in self.arrayButtonsByCategory {
                    if bt.tag == self.selectedCategories {
                        self.tapCategory(sender: bt)
                    }
                }
            }
        }
        
        
        
//        let selectedButton = self.arrayButtonsByCategory[self.selectedCategories]
//        selectedButton.isSelected = true
    }
    
    func showBtLastStickers() {
        if arrayButtonsByCategory.count != 0 {
            for button in arrayButtonsByCategory {
                button.removeFromSuperview()
            }
            self.arrayButtonsByCategory.removeAll()
        }
        self.addButtonsByCategory()
    }
    /*
    // MARK: - GIFs
    
    func downloadGifs() {
        let info = self.getInfoGIFs()
        DispatchQueue.global(qos: .utility).async {
            for (id, urlString) in info {
                if let url = URL(string: "http://cofcmoji.bigdig.com.ua/" + urlString) {
                    self.getDataFromUrl(url: url, completion: { (data, response, error) in
                        if let newData = data {
                            print("save gif \(id)")
                            self.saveImageDocumentDirectory(data: newData, name: "stick\(id).gif")
                            self.collectionView.reloadData()
                        }
                    })
                }
            }
        }
    }
    
    func getInfoGIFs() -> [Int: String] {
        var info: [Int: String] = [:]
        let realm = try! Realm()
        let stickers = realm.objects(Sticker.self)
        for sticker in stickers {
            if let type = sticker.type {
                if type == "gif" && sticker.iconData == nil {
                    info[sticker.id] = sticker.iconUrl
                }
            }
        }
        return info
    }
    */
    /*
    func filterStickers() {
        for (key, _) in self.stickersOfCategory {
            let stickers = self.stickersOfCategory[key]
            let sortedStickers = stickers?.sorted {
                $0.serialNumber < $1.serialNumber
            }
            self.stickersOfCategory[key] = sortedStickers
        }
    }
    */
    /*
    func filterCategory() {
        let _ = self.stickersOfCategory.sorted {
            $0.key < $1.key
        }
    }
    */
    // MARK: - LastSticker
    
    func getLastStickersCount() -> Int {
        let realm = try! Realm()
        let listLastStickers = realm.objects(LastSticker.self)
        print(listLastStickers)
        return listLastStickers.count
        /*
        var i = 0
        for sticker in listLastStickers {
            if sticker.number < 1000 {
                i += 1
            }
        }
        return i
        */
    }
    
    func setLastSticker(stickerId: Int, stickerType: String) {
        let realm = try! Realm()
        
        let lastSticker = realm.objects(LastSticker.self).filter("id = \(stickerId)").first
        
        if let lastSticker = lastSticker {
            try! realm.write {
                realm.delete(lastSticker)
            }
            
            let listLastStickers = realm.objects(LastSticker.self)
            let listLastStickersSort = listLastStickers.sorted {
                $0.number < $1.number
            }
            
            var i = 1
            for lastSticker in listLastStickersSort {
                try! realm.write {
                    lastSticker.number = i
                }
                i += 1
            }
            
            let lastSticker = LastSticker()
            lastSticker.number = 0
            lastSticker.id = stickerId
            lastSticker.type = stickerType
            try! realm.write {
                realm.add(lastSticker)
            }
        } else {
            let listLastStickers = realm.objects(LastSticker.self)
            for lastSticker in listLastStickers {
                try! realm.write {
                    lastSticker.number += 1
                }
            }
            
            let lastSticker = LastSticker()
            lastSticker.number = 0
            lastSticker.id = stickerId
            lastSticker.type = stickerType
            try! realm.write {
                realm.add(lastSticker)
            }
        }
        
        /*
        let listLastStickers = realm.objects(LastSticker.self)
        
        let listLastStickersSort = listLastStickers.sorted {
            $0.number < $1.number
        }
        
        print(listLastStickersSort)
        
        var i = 1
        for lastSticker in listLastStickersSort {
            if lastSticker.id == stickerId {
                try! realm.write {
                    lastSticker.number = 1000
                }
            } else {
                try! realm.write {
                    lastSticker.number = i
                }
                i += 1
            }
        }
        
        let lastSticker = LastSticker()
        lastSticker.number = 0
        lastSticker.id = stickerId
        lastSticker.type = stickerType
        try! realm.write {
            realm.add(lastSticker)
        }
        */
        
        if self.getLastStickersCount() == 1 {
            self.showBtLastStickers()
        }
        
        if self.selectedCategories == 100000 {
            self.collectionView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: false)
            self.collectionView.reloadData()
        }
    }
    
    override func textWillChange(_ textInput: UITextInput?) {
        // The app is about to change the document's contents. Perform any preparation here.
    }
    
    override func textDidChange(_ textInput: UITextInput?) {
        let proxy = self.textDocumentProxy
        if proxy.keyboardAppearance == UIKeyboardAppearance.dark {
            _ = UIColor.white
        } else {
            _ = UIColor.black
        }
    }

    // MARK: - UICollectionView delegate and dataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.selectedCategories == 100000 {
            if self.getLastStickersCount() <= 12 {
                return self.getLastStickersCount()
            } else {
                return 12
            }
        } else {
            if let stickers = self.stickersOfCategory[self.selectedCategories] {
                return stickers.count
            } else {
                return 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StickerCell", for: indexPath) as! StickerCell
        
        var scale = 1
        if UIDevice.current.userInterfaceIdiom == .phone && UIScreen.main.bounds.size.height == 568.0 {
            scale = 3;
        }
        
        if self.selectedCategories == 100000 {
            let realm = try! Realm()
            let lastSticker = realm.objects(LastSticker.self).filter("number = \(indexPath.item)").first
            if let lastSticker = lastSticker {
                if lastSticker.type == "gif" {
                    if let size = self.getSizeImage(name: "stickerThumb\(lastSticker.id).gif") {
                        var processor: ResizingImageProcessor
                        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                            processor = ResizingImageProcessor(targetSize: CGSize(width: (Width - 6 * 30) / 5, height: (size.height * (Width - 6 * 30) / 5) / size.width))
                        } else {
                            processor = ResizingImageProcessor(targetSize: CGSize(width: (Width - 4 * 20) / CGFloat(scale), height: (size.height * (Width - 4 * 20) / CGFloat(scale)) / size.width))
                        }
                        
                        if let url = self.getImage(name: "stickerThumb\(lastSticker.id).gif") {
                            cell.imSticker.image = UIImage.gif(url: "\(url)")
                        }
                        
                        if let urlpath = Bundle.main.path(forResource: "stickerThumb\(lastSticker.id)", ofType: "gif") {
                            let urll = NSURL.fileURL(withPath: urlpath)
                            cell.imSticker.kf.setImage(with: urll, placeholder: nil, options: [.processor(processor)], progressBlock: nil, completionHandler: nil)
                        }
                    } else {
                        if let urlpath = Bundle.main.path(forResource: "stickerThumb\(lastSticker.id)", ofType: "gif") {
                            let urll = NSURL.fileURL(withPath: urlpath)
                            cell.imSticker.image = UIImage.gif(url: "\(urll)")
                        }
                        
                    }
                    cell.stickerType = "gif"
                    
                } else {
                    if let size = self.getSizeImage(name: "sticker\(lastSticker.id).png") {
                        var processor: ResizingImageProcessor
                        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                            processor = ResizingImageProcessor(targetSize: CGSize(width: (Width - 6 * 30), height: (size.height * (Width - 6 * 30)) / size.width))
                        } else {
                            processor = ResizingImageProcessor(targetSize: CGSize(width: (Width - 4 * 20)/CGFloat(scale), height: (size.height * (Width - 4 * 20)/CGFloat(scale)) / size.width))
                        }
                        
                        if let urlpath = Bundle.main.path(forResource: "sticker\(lastSticker.id)", ofType: "png") {
                            let urll = NSURL.fileURL(withPath: urlpath)
                            cell.imSticker.kf.setImage(with: urll, placeholder: nil, options: [.processor(processor)], progressBlock: nil, completionHandler: nil)
                        }
                        
                        if let url = self.getImage(name: "sticker\(lastSticker.id).png") {
                            cell.imSticker.kf.setImage(with: url, placeholder: nil, options: [.processor(processor)], progressBlock: nil, completionHandler: nil)
                        }
                    } else {
                        cell.imSticker.image = UIImage(named: "sticker\(lastSticker.id)")
                    }
                    cell.stickerType = "png"
                }
                
                cell.stickerId = lastSticker.id
            }
            
        } else {
            if let stickers = self.stickersOfCategory[self.selectedCategories] {
                let realm = try! Realm()
                let sticker = realm.objects(Sticker.self).filter("id = \(stickers[indexPath.item])").first
                if let stickerNew = sticker {
                    if stickerNew.type == "gif" {
                        if let size = self.getSizeImage(name: "stickerThumb\(stickerNew.id).gif") {
                            var processor: ResizingImageProcessor
                            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                                processor = ResizingImageProcessor(targetSize: CGSize(width: (Width - 6 * 30) / 5, height: (size.height * (Width - 6 * 30) / 5) / size.width))
                            } else {
                                processor = ResizingImageProcessor(targetSize: CGSize(width: (Width - 4 * 20) / CGFloat(scale), height: (size.height * (Width - 4 * 20) / CGFloat(scale)) / size.width))
                            }
                            
                            if let url = self.getImage(name: "stickerThumb\(stickerNew.id).gif") {
                                cell.imSticker.image = UIImage.gif(url: "\(url)")
                            } else if let urlpath = Bundle.main.path(forResource: "stickerThumb\(stickerNew.id)", ofType: "gif") {
                                let urll = NSURL.fileURL(withPath: urlpath)
                                cell.imSticker.kf.setImage(with: urll, placeholder: nil, options: [.processor(processor)], progressBlock: nil, completionHandler: nil)
                            }
                        } else {
                            if let urlpath = Bundle.main.path(forResource: "stickerThumb\(stickerNew.id)", ofType: "gif") {
                                let urll = NSURL.fileURL(withPath: urlpath)
                                cell.imSticker.image = UIImage.gif(url: "\(urll)")
                            }
                        }

                        cell.stickerType = "gif"
                    } else {
                        if let size = self.getSizeImage(name: "sticker\(stickerNew.id).png") {
                            var processor: ResizingImageProcessor
                            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                                processor = ResizingImageProcessor(targetSize: CGSize(width: (Width - 6 * 30) / 5, height: (size.height * (Width - 6 * 30) / 5) / size.width))
                            } else {
                                processor = ResizingImageProcessor(targetSize: CGSize(width: (Width - 4 * 20) / CGFloat(scale), height: (size.height * (Width - 4 * 20) / CGFloat(scale)) / size.width))
                            }
                            
                            if let urlpath = Bundle.main.path(forResource: "sticker\(stickerNew.id)", ofType: "png") {
                                let urll = NSURL.fileURL(withPath: urlpath)
                                cell.imSticker.kf.setImage(with: urll, placeholder: nil, options: [.processor(processor)], progressBlock: nil, completionHandler: nil)
                            }
                            
                            if let url = self.getImage(name: "sticker\(stickerNew.id).png") {
                                cell.imSticker.kf.setImage(with: url, placeholder: nil, options: [.processor(processor)], progressBlock: nil, completionHandler: nil)
                            }
                        } else {
                            cell.imSticker.image = UIImage(named: "sticker\(stickerNew.id)")
                        }
                        cell.stickerType = "png"
                    }
                    cell.stickerId = stickerNew.id
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! StickerCell
        let pasteBoard = UIPasteboard.general
        if cell.stickerType == "gif" {
            if let url = self.getImage(name: "sticker\(cell.stickerId).gif") {
                do {
                    let data = try Data(contentsOf: url)
                    pasteBoard.setData(data, forPasteboardType: "com.compuserve.gif")
                    self.setLastSticker(stickerId: cell.stickerId, stickerType: "gif")
                    MessageView(screen: self).show()
                } catch {
                    if let urlpath = Bundle.main.path(forResource: "sticker\(cell.stickerId)", ofType: "gif") {
                        let urll = NSURL.fileURL(withPath: urlpath)
                        do {
                            let data = try Data(contentsOf: urll)
                            pasteBoard.setData(data, forPasteboardType: "com.compuserve.gif")
                            self.setLastSticker(stickerId: cell.stickerId, stickerType: "gif")
                            MessageView(screen: self).show()
                        } catch {
                            print(error.localizedDescription)
                        }
                    } else {
                        print(error.localizedDescription)
                    }
                }
            } else {
                if let urlpath = Bundle.main.path(forResource: "sticker\(cell.stickerId)", ofType: "gif") {
                    let urll = NSURL.fileURL(withPath: urlpath)
                    do {
                        let data = try Data(contentsOf: urll)
                        pasteBoard.setData(data, forPasteboardType: "com.compuserve.gif")
                        self.setLastSticker(stickerId: cell.stickerId, stickerType: "gif")
                        MessageView(screen: self).show()
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            }
        } else {
            if let _ = self.getSizeImage(name: "sticker\(cell.stickerId).png") {
                if let url = self.getImage(name: "sticker\(cell.stickerId).png") {
                    do {
                        let data = try Data(contentsOf: url)
                        pasteBoard.image = UIImage(data: data)
                        self.setLastSticker(stickerId: cell.stickerId, stickerType: "png")
                        MessageView(screen: self).show()
                    } catch {
                        print(error.localizedDescription)
                    }
                } else {
                    
                }
            } else {
                if let image = cell.imSticker.image {
                    pasteBoard.image = image
                    self.setLastSticker(stickerId: cell.stickerId, stickerType: "png")
                    MessageView(screen: self).show()
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StickerCell", for: indexPath as IndexPath) as! StickerCell
        cell.imSticker.image = nil
        cell.imSticker.kf.indicatorType = .none
    }
    
    // MARK: - UICollectionView DelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            return CGSize(width: (Width - 6 * 30) / 5, height: (Width - 6 * 30) / 5)
        } else {
            return CGSize(width: (Width - 4 * 20) / 3, height: (Width - 4 * 20) / 3)
        }
    }
    
    // MARK: - Actions
    
    func tapCategory(sender: UIButton){
        self.vwQwertyKeyboard.isHidden = true
        self.selectedCategories = sender.tag
        for bt in self.arrayButtonsByCategory {
            if bt == sender {
                bt.backgroundColor = UIColor(red: 0.31, green: 0.11, blue: 0.07, alpha: 1.0)
            } else {
                bt.backgroundColor = UIColor.clear
            }
        }
    }
    
    // MARK: - QWERTY Keyboard
    
    @IBOutlet var btQ: UIButton!
    @IBOutlet var btW: UIButton!
    @IBOutlet var btE: UIButton!
    @IBOutlet var btR: UIButton!
    @IBOutlet var btT: UIButton!
    @IBOutlet var btY: UIButton!
    @IBOutlet var btU: UIButton!
    @IBOutlet var btI: UIButton!
    @IBOutlet var btO: UIButton!
    @IBOutlet var btP: UIButton!
    
    @IBOutlet var btA: UIButton!
    @IBOutlet var btS: UIButton!
    @IBOutlet var btD: UIButton!
    @IBOutlet var btF: UIButton!
    @IBOutlet var btG: UIButton!
    @IBOutlet var btH: UIButton!
    @IBOutlet var btJ: UIButton!
    @IBOutlet var btK: UIButton!
    @IBOutlet var btL: UIButton!
    
    @IBOutlet var btZ: UIButton!
    @IBOutlet var btX: UIButton!
    @IBOutlet var btC: UIButton!
    @IBOutlet var btV: UIButton!
    @IBOutlet var btB: UIButton!
    @IBOutlet var btN: UIButton!
    @IBOutlet var btM: UIButton!
    
    @IBOutlet var btPoint: UIButton!
    @IBOutlet var btComma: UIButton!
    
    @IBOutlet var btABC: UIButton!
    @IBOutlet var btCL: UIButton!
    @IBOutlet weak var btBS: UIButton!
    
    func setupQWERTYKeyboard() {
        self.btABC.isSelected = false
        let radius: CGFloat = 4
        self.btQ.layer.cornerRadius = radius
        self.btW.layer.cornerRadius = radius
        self.btE.layer.cornerRadius = radius
        self.btR.layer.cornerRadius = radius
        self.btT.layer.cornerRadius = radius
        self.btY.layer.cornerRadius = radius
        self.btU.layer.cornerRadius = radius
        self.btI.layer.cornerRadius = radius
        self.btO.layer.cornerRadius = radius
        self.btP.layer.cornerRadius = radius
        
        self.btA.layer.cornerRadius = radius
        self.btS.layer.cornerRadius = radius
        self.btD.layer.cornerRadius = radius
        self.btF.layer.cornerRadius = radius
        self.btG.layer.cornerRadius = radius
        self.btH.layer.cornerRadius = radius
        self.btJ.layer.cornerRadius = radius
        self.btK.layer.cornerRadius = radius
        self.btL.layer.cornerRadius = radius
        
        self.btZ.layer.cornerRadius = radius
        self.btX.layer.cornerRadius = radius
        self.btC.layer.cornerRadius = radius
        self.btV.layer.cornerRadius = radius
        self.btB.layer.cornerRadius = radius
        self.btN.layer.cornerRadius = radius
        self.btM.layer.cornerRadius = radius
        
        self.btPoint.layer.cornerRadius = radius
        self.btCL.layer.cornerRadius = radius
    }
    
    @IBAction func capsLockPressed(button: UIButton) {
        if !self.btABC.isSelected {
            button.isSelected = !button.isSelected
            if !button.isSelected {
                self.btQ.setTitle("q", for: .normal)
                self.btW.setTitle("w", for: .normal)
                self.btE.setTitle("e", for: .normal)
                self.btR.setTitle("r", for: .normal)
                self.btT.setTitle("t", for: .normal)
                self.btY.setTitle("y", for: .normal)
                self.btU.setTitle("u", for: .normal)
                self.btI.setTitle("i", for: .normal)
                self.btO.setTitle("o", for: .normal)
                self.btP.setTitle("p", for: .normal)
                
                self.btA.setTitle("a", for: .normal)
                self.btS.setTitle("s", for: .normal)
                self.btD.setTitle("d", for: .normal)
                self.btF.setTitle("f", for: .normal)
                self.btG.setTitle("g", for: .normal)
                self.btH.setTitle("h", for: .normal)
                self.btJ.setTitle("j", for: .normal)
                self.btK.setTitle("k", for: .normal)
                self.btL.setTitle("l", for: .normal)
                
                self.btZ.setTitle("z", for: .normal)
                self.btX.setTitle("x", for: .normal)
                self.btC.setTitle("c", for: .normal)
                self.btV.setTitle("v", for: .normal)
                self.btB.setTitle("b", for: .normal)
                self.btN.setTitle("n", for: .normal)
                self.btM.setTitle("m", for: .normal)
            } else {
                self.btQ.setTitle("Q", for: .normal)
                self.btW.setTitle("W", for: .normal)
                self.btE.setTitle("E", for: .normal)
                self.btR.setTitle("R", for: .normal)
                self.btT.setTitle("T", for: .normal)
                self.btY.setTitle("Y", for: .normal)
                self.btU.setTitle("U", for: .normal)
                self.btI.setTitle("I", for: .normal)
                self.btO.setTitle("O", for: .normal)
                self.btP.setTitle("P", for: .normal)
                
                self.btA.setTitle("A", for: .normal)
                self.btS.setTitle("S", for: .normal)
                self.btD.setTitle("D", for: .normal)
                self.btF.setTitle("F", for: .normal)
                self.btG.setTitle("G", for: .normal)
                self.btH.setTitle("H", for: .normal)
                self.btJ.setTitle("J", for: .normal)
                self.btK.setTitle("K", for: .normal)
                self.btL.setTitle("L", for: .normal)
                
                self.btZ.setTitle("Z", for: .normal)
                self.btX.setTitle("X", for: .normal)
                self.btC.setTitle("C", for: .normal)
                self.btV.setTitle("V", for: .normal)
                self.btB.setTitle("B", for: .normal)
                self.btN.setTitle("N", for: .normal)
                self.btM.setTitle("M", for: .normal)
            }

        }
    }
    
    @IBAction func keyPressed(button: UIButton) {
        (textDocumentProxy as UIKeyInput).insertText("\(button.titleLabel!.text!)")
        
//        UIView.animate(withDuration: 0.2, animations: {
//            button.transform = CGAffineTransform.init(scaleX: 2.0, y: 2.0)
//        }, completion: {(_) -> Void in
//            button.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
//        })
    }
    
    @IBAction func backSpacePressed(button: UIButton) {
        textDocumentProxy.adjustTextPosition(byCharacterOffset: 1)
        textDocumentProxy.deleteBackward()
    }

    @IBAction func spacePressed(button: UIButton) {
        (textDocumentProxy as UIKeyInput).insertText(" ")
    }
    
    @IBAction func returnPressed(button: UIButton) {
        (textDocumentProxy as UIKeyInput).insertText("\n")
    }
    
    @IBAction func typeKeyboardPressed(button: UIButton) {
        self.btCL.isSelected = false
        self.btABC.isSelected = !self.btABC.isSelected
        if self.btABC.isSelected {
            self.btQ.setTitle("1", for: .normal)
            self.btW.setTitle("2", for: .normal)
            self.btE.setTitle("3", for: .normal)
            self.btR.setTitle("4", for: .normal)
            self.btT.setTitle("5", for: .normal)
            self.btY.setTitle("6", for: .normal)
            self.btU.setTitle("7", for: .normal)
            self.btI.setTitle("8", for: .normal)
            self.btO.setTitle("9", for: .normal)
            self.btP.setTitle("0", for: .normal)
            
            self.btA.setTitle("-", for: .normal)
            self.btS.setTitle("\\", for: .normal)
            self.btD.setTitle(":", for: .normal)
            self.btF.setTitle(";", for: .normal)
            self.btG.setTitle("(", for: .normal)
            self.btH.setTitle(")", for: .normal)
            self.btJ.setTitle(".", for: .normal)
            self.btK.setTitle("@", for: .normal)
            self.btL.setTitle("\"", for: .normal)
            
            self.btZ.setTitle("?", for: .normal)
            self.btX.setTitle("!", for: .normal)
            self.btC.setTitle("+", for: .normal)
            self.btV.setTitle("=", for: .normal)
            self.btB.setTitle("$", for: .normal)
            self.btN.setTitle("%", for: .normal)
            self.btM.setTitle(",", for: .normal)
        } else {
            self.btQ.setTitle("q", for: .normal)
            self.btW.setTitle("w", for: .normal)
            self.btE.setTitle("e", for: .normal)
            self.btR.setTitle("r", for: .normal)
            self.btT.setTitle("t", for: .normal)
            self.btY.setTitle("y", for: .normal)
            self.btU.setTitle("u", for: .normal)
            self.btI.setTitle("i", for: .normal)
            self.btO.setTitle("o", for: .normal)
            self.btP.setTitle("p", for: .normal)
            
            self.btA.setTitle("a", for: .normal)
            self.btS.setTitle("s", for: .normal)
            self.btD.setTitle("d", for: .normal)
            self.btF.setTitle("f", for: .normal)
            self.btG.setTitle("g", for: .normal)
            self.btH.setTitle("h", for: .normal)
            self.btJ.setTitle("j", for: .normal)
            self.btK.setTitle("k", for: .normal)
            self.btL.setTitle("l", for: .normal)
            
            self.btZ.setTitle("z", for: .normal)
            self.btX.setTitle("x", for: .normal)
            self.btC.setTitle("c", for: .normal)
            self.btV.setTitle("v", for: .normal)
            self.btB.setTitle("b", for: .normal)
            self.btN.setTitle("n", for: .normal)
            self.btM.setTitle("m", for: .normal)
        }
    }
    
    @IBAction func changeLanguage(button: UIButton) {
        self.advanceToNextInputMode()
    }
    
    @IBAction func openStikers(button: UIButton) {
        self.showQWERTYKeyboard()
    }
    
    func isOpenAccessGranted() -> Bool {
        
        if #available(iOSApplicationExtension 10.0, *) {
            UIPasteboard.general.string = "TEST"
            
            if UIPasteboard.general.hasStrings {
                // Enable string-related control...
                UIPasteboard.general.string = ""
                return true
            }
            else
            {
                UIPasteboard.general.string = ""
                return false
            }
        } else {
            // Fallback on earlier versions
            if UIPasteboard.general.isKind(of: UIPasteboard.self) {
                return true
            }else
            {
                return false
            }
            
        }
        
    }
    
    
    //////////////////////////
    // MARK: - QWERTY keyboard
    //////////////////////////
    
    // MARK: - Setup QWERTY keyboard
    
    func setupQwertyKeyboard() {
        self.vwQwertyKeyboardNew = QwertyKeyboardView.instanceFromNib()
        self.vwQwertyKeyboardNew.btNextKeyboard.addTarget(self, action: #selector(handleInputModeList(from:with:)), for: .allTouchEvents)
        self.vwQwertyKeyboardNew.customButtons()
        
        self.vwQwertyKeyboardNew.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(self.vwQwertyKeyboardNew)
        
        self.vwQwertyKeyboardNew.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        self.vwQwertyKeyboardNew.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        self.vwQwertyKeyboardNew.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        self.vwQwertyKeyboardNew.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        self.vwQwertyKeyboardNew.btQ.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btW.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btE.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btR.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btT.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btY.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btU.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btI.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btO.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btP.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        
        self.vwQwertyKeyboardNew.btA.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btS.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btD.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btF.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btG.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btH.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btJ.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btK.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btL.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        
        self.vwQwertyKeyboardNew.btZ.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btX.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btC.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btV.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btB.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btN.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        self.vwQwertyKeyboardNew.btM.addTarget(self, action: #selector(self.didTapBtLetter(sender:)), for: .touchDown)
        
        self.vwQwertyKeyboardNew.btBackspace.addTarget(self, action: #selector(self.didTapBtBackspace), for: .touchDown)
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.didLongTapBtBackspace(sender:)))
        longPressGesture.minimumPressDuration = 1
        self.vwQwertyKeyboardNew.btBackspace.addGestureRecognizer(longPressGesture)
        
        self.vwQwertyKeyboardNew.btNumbers.addTarget(self, action: #selector(self.didTapBtNumbers), for: .touchDown)
        
        self.vwQwertyKeyboardNew.btSpace.addTarget(self, action: #selector(self.didTapBtSpace), for: .touchDown)
        
        self.vwQwertyKeyboardNew.btShift.addTarget(self, action: #selector(self.didTapBtShift), for: .touchDown)
        
        self.vwQwertyKeyboardNew.btReturn.addTarget(self, action: #selector(self.didTapBtReturn), for: .touchDown)
        
        self.vwQwertyKeyboardNew.btStickers.addTarget(self, action: #selector(self.didTapBtStickers), for: .touchDown)
    }
    
    // MARK: - Actions
    
    func didTapBtLetter(sender: UIButton) {
        self.textDocumentProxy.insertText("\(sender.titleLabel!.text!)")
        /*
        if !self.isTapLetter {
            sender.layer.zPosition = 100
            self.textDocumentProxy.insertText("\(sender.titleLabel!.text!)")
            UIView.animate(withDuration: 0.05, animations: {
                sender.frame.origin.x -= 10
                sender.frame.origin.y -= 10
                sender.frame.size.width += 20
                sender.frame.size.height += 20
            }) { (success) in
                if success {
                    sender.frame.origin.x += 10
                    sender.frame.origin.y += 10
                    sender.frame.size.width -= 20
                    sender.frame.size.height -= 20
                    sender.layer.zPosition = 0
                    /*
                    UIView.animate(withDuration: 0.05, animations: {
                        sender.frame.origin.x += 10
                        sender.frame.origin.y += 10
                        sender.frame.size.width -= 20
                        sender.frame.size.height -= 20
                    }) { (success) in
                        if success {
                            sender.layer.zPosition = 0
                        }
                    }
                    */
                }
            }

        }
        */
    }
    
    func didTapBtBackspace() {
        self.textDocumentProxy.deleteBackward()
    }
    
    var isPress: Bool = false
    func didLongTapBtBackspace(sender: UIGestureRecognizer) {
        if sender.state == .began {
            self.isPress = true
            self.backspace()
        } else if sender.state == .ended {
            self.isPress = false
        }
    }
    func backspace() {
        self.textDocumentProxy.deleteBackward()
        if self.isPress {
            self.perform(#selector(self.backspace), with: self, afterDelay: 0.05)
        }
    }
    
    func didTapBtSpace() {
        self.textDocumentProxy.insertText(" ")
    }
    
    func didTapBtReturn() {
        self.textDocumentProxy.insertText("\n")
    }
    
    func didTapBtNumbers() {
        self.vwQwertyKeyboardNew.isNumber = !self.vwQwertyKeyboardNew.isNumber
    }
    
    func didTapBtShift() {
        self.vwQwertyKeyboardNew.isShift = !self.vwQwertyKeyboardNew.isShift
    }
    
    func didTapBtStickers() {
        self.vwQwertyKeyboardNew.isHidden = true
    }
    
    func showVwQwertyKeyboardNew() {
        self.vwQwertyKeyboardNew.isHidden = false
    }
}
