//
//  ServerKeyboard.swift
//  CofCMoji
//
//  Created by Evgeniy Tkachenko on 19.01.17.
//  Copyright © 2017 Big Dig. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift
import AlamofireObjectMapper
import ImageIO
import Kingfisher
import ObjectMapper
import SwiftyJSON

class ServerKeyboard: NSObject {
    
    private static var instance : ServerKeyboard!;
    
    private override init() {
        
    }
    
    class func sharedInstance() -> ServerKeyboard {
        if (self.instance == nil) {
            self.instance = ServerKeyboard();
        }
        return self.instance;
    }
    
    func getAllCategoriesAndStickers(completionBlock: @escaping (_ success: Bool, _ error: Error?, _ categories: [Category], _ stickers: [Sticker]) ->()) {
        let url = "http://api.classatcofc.com/stickers/time?time=0"
        
        self.printRequest(url: url)
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseObject { (response: DataResponse<CategoriesAndStickersResponse>) in
            
            if let error = response.result.error {
                completionBlock(false, error, [Category](), [Sticker]())
            } else if let timeLastUpdateResponse = response.result.value {
                let categories = timeLastUpdateResponse.categories
                let stickers = timeLastUpdateResponse.stickers
                completionBlock(true, nil, categories, stickers)
            }
        }
    }
    
    func printRequest(url: String) {
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            let json = JSON(data: response.data!, options: .mutableContainers, error: nil)
            print(json)
        }
    }
}

// MARK: -

class CategoriesAndStickersResponse: Mappable {
    var categories: [Category] = [Category]()
    var stickers: [Sticker] = [Sticker]()
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        categories <- map["category"]
        stickers <- map["stickers"]
    }
}

