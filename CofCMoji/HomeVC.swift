//
//  ViewController.swift
//  CollegeOfCharleston
//
//  Created by Evgeniy Tkachenko on 19.12.16.
//  Copyright © 2016 BigDig. All rights reserved.
//

import UIKit
import RealmSwift
import CoreData
import Alamofire
import Crashlytics

class HomeVC: UIViewController {

    private let width = UIScreen.main.bounds.size.width
    private let height = UIScreen.main.bounds.size.height
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*
        let standartDefaults: UserDefaults = UserDefaults()
        if standartDefaults.string(forKey: "firstOpen") == nil {
            standartDefaults.set("", forKey: "firstOpen")
            standartDefaults.synchronize()
            self.showError(title: "About Allowing Full Access", message: "Our user’s privacy is a very serious matter. COFCMoji DOES NOT collect any personal information and will NEVER transmit what you type over any network. Turning on “Allow Full Access” is simply an iOS requirement for third party keyboards.")
            
        }
        */
        /*
        Alamofire.request("https://cofcmoji.bigdig.com.ua/stickers/time?time=1488572191").response { response in
            //debugPrint(response.error)
            //debugPrint(response.data)
        }
        */
        /*
        let realm = try! Realm()
        let dateOfUpdate = realm.objects(DateOfUpdate.self).first
        if dateOfUpdate != nil {
            Server.sharedInstance().getTimeLastUpdate(completionBlock: { (success, time, error) in
                if success {
                    if (dateOfUpdate?.dateOfUpdate)! < time! {
                        Server.sharedInstance().getLastCategoriesAndStickers(time: (dateOfUpdate?.dateOfUpdate)!, completionBlock: { (success, error) in
                            if success {
                                let newDateOfUpdate = DateOfUpdate()
                                newDateOfUpdate.id = 0
                                newDateOfUpdate.dateOfUpdate = Int(Date().timeIntervalSince1970 + 120)
                                try! realm.write {
                                    realm.add(newDateOfUpdate, update: true)
                                }
                            } else {
                                self.showError(title: "Error Internet connection!", message: error)
                            }
                        })
                    }
                } else {
                    self.showError(title: "Error Internet connection!", message: error)
                }
            })
        } else {
            Server.sharedInstance().getAllCategoriesAndStickers(completionBlock: { (success, error) in
                if success {
                    let newDateOfUpdate = DateOfUpdate()
                    newDateOfUpdate.id = 0
                    newDateOfUpdate.dateOfUpdate = Int(Date().timeIntervalSince1970 + 120)
                    try! realm.write {
                        realm.add(newDateOfUpdate, update: true)
                    }
                } else {
                    self.showError(title: "Error Internet connection!", message: error)
                }
            })
        }
        */
        /*
        let frame = CGRect(x: (self.width - 100) / 2, y: (self.height - 100) / 2, width: 100, height: 100)
        let color = UIColor.white
        self.loader = NVActivityIndicatorView(frame: frame, type: .ballSpinFadeLoader, color: color, padding: 1)
        self.view.addSubview(self.loader)
        
        self.loader.startAnimating()
        
        self.perform(#selector(self.deleteLoader), with: self, afterDelay: 15)
        
        let realm = try! Realm()
        Server.sharedInstance().getAllCategoriesAndStickers(completionBlock: { (success, error) in
            if success {
                let newDateOfUpdate = DateOfUpdate()
                newDateOfUpdate.id = 0
                newDateOfUpdate.dateOfUpdate = Int(Date().timeIntervalSince1970 + 120)
                try! realm.write {
                    realm.add(newDateOfUpdate, update: true)
                }
            } else {
                self.showError(title: "Error Internet connection!", message: error)
                self.deleteLoader()
            }
        })
        */
        
        /*
        let button = UIButton(type: .roundedRect)
        button.frame = CGRect(x: 20, y: 50, width: 100, height: 30)
        button.setTitle("Crash", for: [])
        button.addTarget(self, action: #selector(self.crashButtonTapped(_:)), for: .touchUpInside)
        view.addSubview(button)
        */
        
    }
    
    @IBAction func crashButtonTapped(_ sender: AnyObject) {
        Crashlytics.sharedInstance().crash()
    }
    
    func showError(title: String, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

