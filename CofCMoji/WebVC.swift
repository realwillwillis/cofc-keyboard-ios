//
//  WebVC.swift
//  CofCMoji
//
//  Created by Evgeniy Tkachenko on 10.01.17.
//  Copyright © 2017 Big Dig. All rights reserved.
//

import UIKit

class WebVC: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        
        let url = URL (string: "http://classatcofc.com/emoji.html")
        
        let requestObj = NSURLRequest(url: url!);
        webView.loadRequest(requestObj as URLRequest)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    @IBAction func btBack(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
}
