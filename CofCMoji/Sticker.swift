//
//  Sticker.swift
//  CollegeOfCharleston
//
//  Created by Evgeniy Tkachenko on 30.12.16.
//  Copyright © 2016 BigDig. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import Realm

class Sticker: Object, Mappable {
    dynamic var id = 0
    dynamic var categoryId = 0
    dynamic var timestamp = 0
    dynamic var isHide = false
    dynamic var serialNumber = 0
    dynamic var iconUrl = ""
    dynamic var iconData: Data?
    dynamic var type: String?
    dynamic var iconThumbUrl = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        id <- map["id"]
        categoryId <- map["category_id"]
        timestamp <- map["timestamp"]
        isHide <- map["is_hide"]
        serialNumber <- map["serial_number"]
        iconUrl <- map["icon"]
        iconThumbUrl <- map["thumb_gif"]
    }
    
    func downloadImage(completion: @escaping (_ success: Bool) -> ()) {
        if let url = URL(string: "http://api.classatcofc.com/" + self.iconUrl) {
            getDataFromUrl(url: url) { (data, response, error)  in
                guard let data = data, error == nil else { return }
                self.iconData = data
                completion(true)
            }
        } else {
            print("Error")
            completion(false)
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> ()) {
        URLSession.shared.dataTask(with: url) {(data, response, error) in
            completion(data, response, error)
            }.resume()
    }
 
}
